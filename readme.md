This is a simple number class and API, created as an exercise.  To use the API, you must have AllowOverride = ALL enabled in your web server for
the directory in which the API resides (to enable translation of the .php extension to a simple URL).  

To use the API, pass a POST with an array of numbers as JSON as follows:
{

  "numbers": [

     5, 6, 8, 7, 5

  ]

}

The API will return the mean, median, mode and range as JSON:

{

  "results": {

     "mean": 6.2,

     "median": 6,

     "mode": 5,

     "range": 3

  }

}

Any other posts to the API will result in an error, as will any other action to the API.  All internal errors will result in 500 errors.  
If there are no results for any of the above values, an empty string will be returned.


