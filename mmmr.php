<?php
require_once 'numbers.php';
$numbers = new Numbers;
if ( !empty ($_GET)) {
        $errors = array ('code' => '404',
		         'message' => 'Method GET not available on this endpoint');
	$errorArray  = array ('error' => $errors );
	$data = $errorArray;	
}
elseif ( empty ($_POST)) {
        $errors = array ('code' => '404',
		         'message' => 'You must provide a parameter to this endpoint');
	$errorArray  = array ('error' => $errors );
	$data = $errorArray;	
}

elseif ( !(isset($_POST['numbers']))){ 
        $errors = array ('code' => '404',
		         'message' => 'You must provide a parameter called numbers to this endpoint');
	$errorArray  = array ('error' => $errors );
	$data = $errorArray;	
}
else {
	$numberJson = $_POST['numbers'];
	$numberObject= json_decode ($numberJson);
	$numberArray = (array)$numberObject;
	$numbersArray = $numberArray['numbers'];
	$mean = $numbers->mean($numbersArray);
	$median = $numbers->median($numbersArray);
	$mode = $numbers->mode($numbersArray);
	$range = $numbers->mode($numbersArray);
	$results = array ( 'mean' => $mean, 
	          'median' => $median,
		   'mode' => $mode,
		   'range' => $range);
	$data = array ( 'results' => $results);
}
register_shutdown_function(function() {
    $lastError = error_get_last();

    if (!empty($lastError) && $lastError['type'] == E_ERROR) {
         $errors = array ('code' => '500',
		         'message' => 'Internal Server Error');
	$errorArray  = array ('error' => $errors );
	$data = $errorArray;       
    }
});
header('Content-Type: application/json');
echo json_encode($data);

?>
