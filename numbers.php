<?php
class Numbers 
{
/* A simple php library to calculate the mean, median, mode, 
* and range of numbers
*/

/*This function calculates the mean of an array of numbers
* @PARAM numbers array numbers to calculate the mean of
* @RETURN mean float the mean of the numbers
*/
public function mean ($numbers){
	 if( !is_array($numbers) ){
        	
		return NULL;
         } 
	 $count = count($numbers);
         $sum = array_sum($numbers);
         $mean = $sum / $count;
	 if ($mean != 0 && $mean != NULL ) { 
	 	return round($mean, 3); 
	 }
	 else {
            return NULL;
	 }
}


/*This function calculates the median of an array of numbers
* @PARAM numbers array numbers to calculate the median of
* @RETURN float the median of the numbers
*/
public function median ($numbers) {
	if( !is_array($numbers) ){
        	return NULL;
         } 
         rsort($numbers);
                $middle = round(count($numbers) / 2);
                $median = $numbers[$middle-1]; 	 
	if ($median != 0 && $median != NULL ) { 
	 	return round($median,3 ); 
	 }
	 else {
            return NULL;
	 }
}

/*This function calculates the mode of an array of numbers
* @PARAM numbers array numbers to calculate the mode of
* @RETURN float the mode of the numbers
*/
public function mode ($numbers) {
	if( !is_array($numbers) ){
        	return NULL;
         } 
         $v = array_count_values($numbers);
         arsort($v);
         foreach($v as $k => $v){
		$mode = $k; 
		break;
	}         
	if ($mode != 0 && $mode != NULL ) { 
	 	return round($mode, 3 ); 
	 }
	 else {
            return NULL;
	 }
}

/*This function calculates the range of an array of numbers
* @PARAM numbers array numbers to calculate the range of
* @RETURN float the range of the numbers
*/
public function range ($numbers) {
	if( !is_array($numbers) ){
        	return NULL;
         } 
        sort($numbers);
        $sml = $numbers[0];
        rsort($numbers);
        $lrg = $numbers[0];
        $range = $lrg - $sml;  
	if ($range != 0 && $range != NULL ) { 
	 	return round($range ,3 ); 
	 }
	 else {
            return NULL;
	 }
}

}
